# اپلیکیشن پیاده سازی Button navigation 

در این اپلیکیشن سعی شده که طراحی Button navigation با استفاده از پکیج مدیریت route مشهور به با نام auto route پیاده سازی شود .

auto route :

پکیج auto_route یک پکیج ناوبری در فلاتر است که به برنامه نویسان کمک می‌کند تا به راحتی صفحات و مسیرهای ناوبری مورد نیاز در برنامه‌هایشان را تعریف کنند. با استفاده از این پکیج، می‌توانید بسیاری از مسائل مرتبط با ناوبری در برنامه‌های فلاتر خود را حل کنید.

این پکیج به شما امکان می‌دهد تا صفحات مختلفی را برای برنامه خود تعریف کنید و به صورت اتوماتیک مسیرهای ناوبری مورد نیاز بین آنها را ایجاد کنید. همچنین شما می‌توانید پارامترهای مختلفی را به این مسیرهای ناوبری اضافه کنید و آنها را به راحتی در دسترس قرار دهید.


## نصب و راه‌اندازی

1. ابتدا فایل پروژه را کلون کنید:

`git clone https://gitlab.com/farzinkarimi/auto_route_with_button_nav.git`

## نصب پکیج ها

2. در فایل pubspec.yaml پروژه، به قسمت dependencies اضافه کنید:

```
dependencies:
  auto_route: ^2.2.0
  ```
سپس از طریق دستور flutter pub get پکیج را نصب کنید.


3. سپس، اپلیکیشن را اجرا کنید:
```
flutter run
```
## استفاده از اپلیکیشن
برنامه شامل دوصفحه Home و Setting می باشد که در صفحه home دو پست وجود دارد که یکی از آنها بصورت customRoute پیاده سازی شده و دیگری به صورت NamedRoute پیاده سازی شده است. 

<p align="center">
<img  src="https://gitlab.com/farzinkarimi/auto_route_with_button_nav/-/raw/main/screenshots/Screenshot_1680167019.png"   alt= “” width="50%"  height="100%" text-align:center display: block>
</p>




در قسمت پیاده سازی بصورت customRoute ما میتوانیم انیمیشن صفحه خارج شده و صفحه وارد شده را کنترل کنیم و همچنین یک متغیر انیمیشن را که از لحظه ایجاد صفحه دوم در دسترس است با استفاده از پکیج Provider در اختیار صفحه قرار می دهیم.

cupertino_route.dart

```
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CupertinoRoutex extends PageRouteBuilder {
  final Widget enterPage;
  final Widget exitPage;
  final PageTransitionsBuilder matchingBuilder =
      const CupertinoPageTransitionsBuilder(); // Default iOS/macOS (to get the swipe right to go back gesture)



  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return matchingBuilder.buildTransitions(
        this, context, animation, secondaryAnimation, child);
  }

  CupertinoRoutex({required this.exitPage, required this.enterPage})
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) {
            return ListenableProvider(
                create: (context) => animation, child: enterPage);
          },
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            return Stack(
              children: <Widget>[
                SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(0.0, 0.0),
                    end: const Offset(-0.33, 0.0),
                  ).animate(
                    CurvedAnimation(
                      parent: animation,
                      curve: Curves.decelerate,
                      reverseCurve: Curves.easeIn,
                    ),
                  ),
                  child: exitPage,
                ),
                SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1.0, 0.0),
                    end: Offset.zero,
                  ).animate(
                    CurvedAnimation(
                      parent: animation,
                      curve: Curves.easeInCubic,
                      reverseCurve: Curves.easeInCubic,
                    ),
                  ),
                  child: enterPage,
                )
              ],
            );
          },
        );

}

```

در صفحه پست هم یک دکمه وجود دارد که اگر برروی آن کلیک کنید پیغام موفق نمایش داده می شود اما درصورتی که با سوایپ کردن به صفحه اول برگردید هیچ پیغامی مشاهده نمی کنید.

<img  src="https://gitlab.com/farzinkarimi/auto_route_with_button_nav/-/raw/main/screenshots/Screenshot_1680167126.png"   alt= “” width="50%"  height="100%" text-align:center display: block>

در نهایت در صفحه setting با استفاده از پکیج styled_widget یک طراحی از صفحه تنظیمات پیاده سازی شده است.


<img  src="https://gitlab.com/farzinkarimi/auto_route_with_button_nav/-/raw/main/screenshots/Screenshot_1680167023.png"   alt= “” width="50%"  height="100%" text-align:center display: block>

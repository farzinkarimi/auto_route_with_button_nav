import 'package:auto_route_bottom_nav/pages/main_page.dart';
import 'single_widget_page.dart';
import 'package:auto_route_bottom_nav/routes/cupertino_route.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < HomePosts.posts.length; i++)
          Container(
            color: Color(int.parse(HomePosts.posts[i]["color"]!)),
            child: ListTile(
                title: Center(child: Text(HomePosts.posts[i]["title"]!)),
                onTap: () {
                  if (i % 2 == 0) {
                    Navigator.of(context).push(CupertinoRoutex(
                        exitPage: this, enterPage: SingleWidgetPage(id: i)));
//                      context.router.push(SinglePostRouter(id: i)).then((value) {
//                        if (value != null) {
//                          Fluttertoast.showToast(
//                            msg: value.toString(),
//                          );
//                        }
//                      });
                  } else {
                    AutoRouter.of(context)
                        .pushNamed(
                      "home/$i",
                    )
                        .then((value) {
                      if (value != null) {
                        Fluttertoast.showToast(msg: value.toString());
                      }
                    });
                  }
                }),
          ),
      ],
    );
  }
}

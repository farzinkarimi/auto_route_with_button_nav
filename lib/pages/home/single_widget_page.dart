import 'package:auto_route_bottom_nav/pages/main_page.dart';
import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:provider/provider.dart';

class SingleWidgetPage extends StatelessWidget {
  final int id;

  const SingleWidgetPage({Key? key, @PathParam('singleId') required this.id})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final animation = Provider.of<Animation<double>>(context, listen: false);
    return Container(
      color: Color(
        int.parse(
          HomePosts.posts[id]["color"]!,
        ),
      ),
      child: AnimatedBuilder(
        animation: animation,
        builder: (context, child) => SlideTransition(
          position: animation.drive(
            Tween<Offset>(begin: const Offset(1, 0), end: const Offset(0, 0)).chain(
              CurveTween(curve: Curves.decelerate),
            ),
          ),
          child: child,
        ),
        child: Center(
          child: ElevatedButton(
              onPressed: () async {
                context.popRoute("success");
              },
              child: const Text("back with Awaiting results")),
        ),
      ),
    );
  }
}

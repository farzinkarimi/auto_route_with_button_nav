
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:styled_widget/styled_widget.dart';

class ViewProfilePage extends StatelessWidget {
  const ViewProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final animation = Provider.of<Animation<double>>(context, listen: false);
    return Scaffold(
      backgroundColor: Colors.lightBlueAccent,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(
          MediaQuery.of(context).size.height * 0.3,
        ),
        child: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.lightBlueAccent,
          elevation: 0,
          flexibleSpace: Hero(
            createRectTween: _createRectTween,
            transitionOnUserGestures: true,
            tag: "appBarBack",
            child: Container(
              color: Theme.of(context).primaryColor,
            ),
          ),
          leading: Align(
            alignment: Alignment.topLeft,
            child: AnimatedBuilder(
              animation: animation,
              builder: (context, child) => FadeTransition(
                opacity: animation,
                child: child,
              ),
              child: TextButton.icon(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon:const Icon(
                    Icons.chevron_left,
                    color: Colors.white,
                  ),
                  label:const  Text(
                    "back",
                  ).textColor(Colors.white)),
            ),
          ),
          centerTitle: true,
          toolbarHeight: MediaQuery.of(context).size.height * 0.3,
          title: SizedBox(
            height: MediaQuery.of(context).size.height * 0.3,
            child: Column(
              children: [
                const    SizedBox(
                  height: 16,
                ),
                Expanded(
                  flex: 2,
                  child: Hero(
                    tag: "avatar",
                    child: ClipOval(
                      child: Image.asset(
                        "assets/avatar.jpg",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                const  SizedBox(
                  height: 12,
                ),
                Expanded(
                  child: Hero(
                    tag: "text",
                    child: Material(
                      color: Colors.transparent,
                      child:const  Text(
                        "animation test",
                      ).textColor(Colors.white).fontSize(18),
                    ),
                  ),
                )
              ],
            ),
          ),
          actions: [
            Align(
              alignment: Alignment.topRight,
              child: TextButton(
                  onPressed: () {},
                  child:const  Text(
                    "edit",
                  ).textColor(Colors.white)),
            )
          ],
          leadingWidth: 80,

        ),
      ),
      body: Container(
        color: Colors.lightBlueAccent,
      ),
    );
  }
}

RectTween _createRectTween(Rect? begin, Rect? end) {
  return CustomRectTween(begin: begin!, end: end!);
}

class CustomRectTween extends RectTween {
  CustomRectTween({required Rect begin, required Rect end})
      : super(begin: begin, end: end);

  @override
  Rect lerp(double t) {
    double height = end!.height - begin!.height;

    // Cubic easeOutQuad = const Cubic(0.25, 0.46, 0.45, 0.94);
    // double transformedY = easeOutQuad.transform(t);

    double animatedY = begin!.height + (t * height);

    return Rect.fromLTWH(0, 0, end!.width, animatedY);
  }
}

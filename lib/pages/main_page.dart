import 'package:auto_route/auto_route.dart';
import 'package:auto_route_bottom_nav/pages/viewProfile/view_profile_page.dart';
import 'package:auto_route_bottom_nav/routes/cupertino_route.dart';
import 'package:auto_route_bottom_nav/routes/main_route.gr.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:styled_widget/styled_widget.dart';
class HomePosts {
  static const posts = [
    {"title": "post 1", "color": "0xFF43A047"},
    {"title": "post 2", "color": "0xFF087ABE"}
  ];
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AutoTabsScaffold(
      appBarBuilder: (context, tabsRouter) => PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: AppBar(
          flexibleSpace: Hero(
              tag: "appBarBack",
              child: Container(color: Theme.of(context).primaryColor,)),
          centerTitle: true,
          title:Hero(
            tag: "text",
            child: Material(
              color: Colors.transparent,

              child: Text(
                "animation test",
              ).textColor(Colors.white).fontSize(18),
            ),
          ),
          actions: [
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(
                  CupertinoRoutex(
                    exitPage: this,
                    enterPage: ViewProfilePage(),
                  ),
                );
              },
              
              child: Hero(
                tag: "avatar",
                child: ClipOval(
                  child: Image.asset("assets/avatar.jpg"),
                ),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBuilder: (context, tabsRouter) => SalomonBottomBar(
          currentIndex: tabsRouter.activeIndex,
          onTap: tabsRouter.setActiveIndex,
          items: [
            SalomonBottomBarItem(
              selectedColor: Colors.amberAccent,
              icon: const Icon(
                EvaIcons.homeOutline,
                size: 30,
              ),
              title: const Text('Home'),
            ),
            SalomonBottomBarItem(
              selectedColor: Colors.blue[200],
              icon: const Icon(
                EvaIcons.settings2,
                size: 30,
              ),
              title: const Text('Setting'),
            ),
          ]),
      routes: const [
        HomeRouter(),
        SettingsRouter(),
      ],
    );
  }
}

Widget _flightShuttleBuilder(
  BuildContext flightContext,
  Animation<double> animation,
  HeroFlightDirection flightDirection,
  BuildContext fromHeroContext,
  BuildContext toHeroContext,
) {
  return DefaultTextStyle(
    style: DefaultTextStyle.of(toHeroContext).style,
    child: toHeroContext.widget,
  );
}

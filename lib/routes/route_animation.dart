import 'package:animations/animations.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SharedAxisTransitionPageWrapper<T> extends AutoRoutePage<T> {
  final int durationInMilliseconds;
  final int reverseDurationInMilliseconds;
  final CustomRouteBuilder? customRouteBuilder;

  SharedAxisTransitionPageWrapper({
    required RouteData routeData,
    required Widget child,
    LocalKey? key,
    bool fullscreenDialog = false,
    bool maintainState = true,
    this.durationInMilliseconds = 600,
    this.reverseDurationInMilliseconds = 600,
    this.customRouteBuilder,
  }) : super(
          routeData: routeData,
          key: key,
          child: child,
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
        );

  @override
  Route<T> onCreateRoute(BuildContext context) {
    return PageRouteBuilder<T>(
      settings: this,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        return SharedAxisTransition(
          fillColor: Theme.of(context).cardColor,
          animation: animation,
          secondaryAnimation: secondaryAnimation,
          transitionType: SharedAxisTransitionType.vertical,
          child: child,
        );
      },
      pageBuilder: (context, animation, secondaryAnimation) {
        return ListenableProvider(create: (_) => animation,child: buildPage(context),);
      },
      reverseTransitionDuration: const Duration(milliseconds: 600)

    );
  }

}

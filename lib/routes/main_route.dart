import 'package:auto_route/empty_router_widgets.dart';
import 'package:auto_route_bottom_nav/pages/viewProfile/view_profile_page.dart';

import '../pages/home/home_page.dart';
import 'package:auto_route_bottom_nav/pages/main_page.dart';

import '../pages/setting/setting_page.dart';
import '../pages/home/single_widget_page.dart';

import 'package:auto_route/auto_route.dart';

@AdaptiveAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: [
    RedirectRoute(path: '/', redirectTo: 'main'),
    AutoRoute(
      name: "mainRouter",
      path: "main",
      page: MainPage,
      children: [
            RedirectRoute(path: '/', redirectTo: 'main'),
        CustomRoute(

            name: "homeRouter",
            path: "home",
            page: EmptyRouterPage,
            children: [
              CustomRoute(path: '', page: HomePage,),
              CustomRoute<String>(
                  name: "singlePostRouter",
                  path: "home/:singleId",
                  page: SingleWidgetPage),
              RedirectRoute(path: '*', redirectTo: ''),
            ]),
        AutoRoute(
          path: 'settings',
          name: 'SettingsRouter',
          page: SettingPage,

        ),
      ],
    ),
    CustomRoute(
        path: 'viewProfile',
        name: 'viewProfileRouter',
        page: ViewProfilePage)
  ],
)
class $AppRouter {}

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

import 'package:auto_route/auto_route.dart' as _i7;
import 'package:auto_route/empty_router_widgets.dart' as _i3;
import 'package:auto_route_bottom_nav/pages/home/home_page.dart' as _i5;
import 'package:auto_route_bottom_nav/pages/home/single_widget_page.dart'
    as _i6;
import 'package:auto_route_bottom_nav/pages/main_page.dart' as _i1;
import 'package:auto_route_bottom_nav/pages/setting/setting_page.dart' as _i4;
import 'package:auto_route_bottom_nav/pages/viewProfile/view_profile_page.dart'
    as _i2;
import 'package:auto_route_bottom_nav/routes/route_animation.dart';
import 'package:flutter/material.dart' as _i8;

class AppRouter extends _i7.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    MainRouter.name: (routeData) {
      return _i7.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i1.MainPage());
    },
    ViewProfileRouter.name: (routeData) {
      return _i7.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i2.ViewProfilePage(),
          opaque: true,
          barrierDismissible: false);
    },
    HomeRouter.name: (routeData) {
      return _i7.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i3.EmptyRouterPage(),
          opaque: true,
          barrierDismissible: false);
    },
    SettingsRouter.name: (routeData) {
      return _i7.AdaptivePage<dynamic>(
          routeData: routeData, child: const _i4.SettingPage());
    },
    HomeRoute.name: (routeData) {
      return _i7.CustomPage<dynamic>(
          routeData: routeData,
          child: const _i5.HomePage(),
          opaque: true,
          barrierDismissible: false);
    },
    SinglePostRouter.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<SinglePostRouterArgs>(
          orElse: () =>
              SinglePostRouterArgs(id: pathParams.getInt('singleId')));
      return SharedAxisTransitionPageWrapper(
          routeData: routeData,
          child: _i6.SingleWidgetPage(key: args.key, id: args.id));
          
    }
  };

  @override
  List<_i7.RouteConfig> get routes => [
        _i7.RouteConfig('/#redirect',
            path: '/', redirectTo: 'main', fullMatch: true),
        _i7.RouteConfig(MainRouter.name, path: 'main', children: [
          _i7.RouteConfig('/#redirect',
              path: '/',
              parent: MainRouter.name,
              redirectTo: 'main',
              fullMatch: true),
          _i7.RouteConfig(HomeRouter.name,
              path: 'home',
              parent: MainRouter.name,
              children: [
                _i7.RouteConfig(HomeRoute.name,
                    path: '', parent: HomeRouter.name),
                _i7.RouteConfig(SinglePostRouter.name,
                    path: 'home/:singleId', parent: HomeRouter.name),
                _i7.RouteConfig('*#redirect',
                    path: '*',
                    parent: HomeRouter.name,
                    redirectTo: '',
                    fullMatch: true)
              ]),
          _i7.RouteConfig(SettingsRouter.name,
              path: 'settings', parent: MainRouter.name)
        ]),
        _i7.RouteConfig(ViewProfileRouter.name, path: 'viewProfile')
      ];
}

/// generated route for
/// [_i1.MainPage]
class MainRouter extends _i7.PageRouteInfo<void> {
  const MainRouter({List<_i7.PageRouteInfo>? children})
      : super(MainRouter.name, path: 'main', initialChildren: children);

  static const String name = 'MainRouter';
}

/// generated route for
/// [_i2.ViewProfilePage]
class ViewProfileRouter extends _i7.PageRouteInfo<void> {
  const ViewProfileRouter()
      : super(ViewProfileRouter.name, path: 'viewProfile');

  static const String name = 'ViewProfileRouter';
}

/// generated route for
/// [_i3.EmptyRouterPage]
class HomeRouter extends _i7.PageRouteInfo<void> {
  const HomeRouter({List<_i7.PageRouteInfo>? children})
      : super(HomeRouter.name, path: 'home', initialChildren: children);

  static const String name = 'HomeRouter';
}

/// generated route for
/// [_i4.SettingPage]
class SettingsRouter extends _i7.PageRouteInfo<void> {
  const SettingsRouter() : super(SettingsRouter.name, path: 'settings');

  static const String name = 'SettingsRouter';
}

/// generated route for
/// [_i5.HomePage]
class HomeRoute extends _i7.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: '');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i6.SingleWidgetPage]
class SinglePostRouter extends _i7.PageRouteInfo<SinglePostRouterArgs> {
  SinglePostRouter({_i8.Key? key, required int id})
      : super(SinglePostRouter.name,
            path: 'home/:singleId',
            args: SinglePostRouterArgs(key: key, id: id),
            rawPathParams: {'singleId': id});

  static const String name = 'SinglePostRouter';
}

class SinglePostRouterArgs {
  const SinglePostRouterArgs({this.key, required this.id});

  final _i8.Key? key;

  final int id;

  @override
  String toString() {
    return 'SinglePostRouterArgs{key: $key, id: $id}';
  }
}
